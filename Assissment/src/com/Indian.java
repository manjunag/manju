package com;

public  class Indian implements Comparable<Indian>{
	Double cost;
	Indian(double cost){
		this.cost=cost;
	}
	public String toString() {
		return "Cost:"+cost;
	}
	
	public int compareTo(Indian i) {
		return i.cost.compareTo(this.cost);
	}
}
