package com;

import java.util.TreeSet;

public class SortByCost {
	public static void main(String[] args) {
		TreeSet<Indian> t = new TreeSet<Indian>();
		Indian i1 =new Indian(20000.00);
		Indian i2 =new Indian(10000.00);
		Indian i3 =new Indian(30000.00);

		t.add(i1);
		t.add(i2);
		t.add(i3);
	
		System.out.println(t.first()+" Least amount required to buy all the product");
			
		
	}
}

